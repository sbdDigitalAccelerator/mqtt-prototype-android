package com.example.weston.mqtt_prototype_android

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Contacts
import android.util.Log
import android.widget.TextView

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttClientStatusCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttNewMessageCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos;
import com.amazonaws.regions.Regions;
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import java.nio.charset.Charset
import kotlin.coroutines.experimental.suspendCoroutine

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val CUSTOMER_SPECIFIC_ENDPOINT = "a1djrdv8py9ovi.iot.us-east-1.amazonaws.com"
        val COGNITO_POOL_ID = "us-east-1:d34658a7-683f-4dc6-8996-9c25bb562c7d"
        val REGION = Regions.US_EAST_1;

        val clientId = UUID.randomUUID().toString();

        val credentialsProvider = CognitoCachingCredentialsProvider(
                applicationContext,
                COGNITO_POOL_ID,
                REGION
        )

        val mqttManager = AWSIotMqttManager(clientId, CUSTOMER_SPECIFIC_ENDPOINT)

        async {
            async{
                connect(mqttManager, credentialsProvider)
            }.await()
            subscribe(mqttManager)
        }
    }

    suspend fun connect(mqttManager: AWSIotMqttManager, credentialsProvider: CognitoCachingCredentialsProvider) {
        suspendCoroutine<Unit> { cont ->
            try {
                mqttManager.connect(credentialsProvider) { status, throwable ->
                    async(UI) {
                        when (status) {
                            AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.Connecting ->
                                connection.text = "Connecting..."
                            AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.Connected -> {
                                connection.text = "Connected"
                                cont.resume(Unit)
                            }
                            else -> null

                        }
                    }
                }
            } catch (e: Exception) {
                cont.resumeWithException(e)
            }
        }

    }

    fun subscribe(mqttManager: AWSIotMqttManager) {
        try {
            mqttManager.subscribeToTopic("myTopic", AWSIotMqttQos.QOS0) { topic, data ->
                async(UI) {
                    val messageText = String(data, Charset.forName("UTF-8"))
                    message.text =
                            "${message.text} $topic: $messageText\n"
                }
            }
        } catch(e: Error) {
            Log.e("error", e.localizedMessage)
        }
    }

    fun publishMessage(mqttManager: AWSIotMqttManager) {
        try {
            mqttManager.publishString("blah", "myTopic", AWSIotMqttQos.QOS0)
        } catch(e: Error) {
            Log.e("error", e.localizedMessage)
        }
    }
}
